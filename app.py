import tensorflow as tf
from flask import Flask, request, jsonify
import base64
import json
app = Flask(__name__)


@app.route('/upload', methods=['POST'])
def upload_base64_file():
    data = request.get_json()
    #print(data)

    if data is None:
        
        
        print("No valid request body, json missing!")
        return jsonify({'error': 'No valid request body, json missing!'})
    else:

        img_data = data['img']

        # this method convert and save the base64 string to image
        convert_and_save(img_data)
        (labels_name, labels_score) = classify("imageToSave.png")
        with open('frutis_veg_details.json') as f:
            data = json.load(f)
        frut_veg_info = data[labels_name]
        labels_name = labels_name.replace("3","").capitalize().trim()
        return jsonify({'success': 'Success Uploaded', 'label_name': labels_name, 'label_score': str(labels_score),'frut_veg_info': frut_veg_info})
        #return ""

@app.route('/')
def index():
    return 'Web App with Python Flask!'


def convert_and_save(b64_string):
    with open("imageToSave.png", "wb") as fh:
        fh.write(base64.decodebytes(b64_string.encode()))


def classify(image_path):
    # change this as you see fit
    labels_name = []
    labels_score = []
    # Read in the image_data
    image_data = tf.gfile.FastGFile(image_path, 'rb').read()

    # Loads label file, strips off carriage return
    label_lines = [line.rstrip() for line
                   in tf.gfile.GFile("tf_files/retrained_labels.txt")]

    # Unpersists graph from file
    with tf.gfile.FastGFile("tf_files/retrained_graph.pb", 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')

    with tf.Session() as sess:
        # Feed the image_data as input to the graph and get first prediction
        softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

        predictions = sess.run(softmax_tensor,
                               {'DecodeJpeg/contents:0': image_data})

        # Sort to show labels of first prediction in order of confidence
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]

        for node_id in top_k:
            human_string = label_lines[node_id]
            score = predictions[0][node_id]
            labels_name.append(human_string)
            labels_score.append(score)
            print('%s (score = %.5f)' % (human_string, score))
        print(labels_score)
        print(labels_name)
        return (labels_name[0], labels_score[0])


app.run(host='0.0.0.0', port=80)
